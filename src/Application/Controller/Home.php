<?php
namespace Application\Controller;

use vimeo\Controller,
    vimeo\Common,
    vimeo\Session;

require_once SYSTEM_PATH . 'vendor/vimeo/vendor/autoload.php';

class Home extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelHome', 'model');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }
    }

    public function main()
    {
        $dados = [];
        parent::prepararView("Home/pag_home", $dados);
    }

    public function UploadVideo()
    {
        $client = new \Vimeo(APP_VIMEO, CLIENT_VIMEO, CLIENT_SECRET_VIMEO);

        $file_name = "{path to a video on the file system}";
        $uri = $client->upload($file_name, array(
            "name" => "Untitled",
            "description" => "The description goes here."
        ));

        echo "Your video URI is: " . $uri;
    }
}
