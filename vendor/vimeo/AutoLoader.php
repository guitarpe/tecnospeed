<?php
namespace vimeo;

class AutoLoader
{

    public $diretorios = [];

    public function register()
    {
        spl_autoload_register([$this, 'loader']);
    }

    private function loader($className)
    {

        if (strstr($className, '\\')) {
            $class = str_replace('\\', DIRECTORY_SEPARATOR, ltrim($className, '\\')) . '.php';
        } else {
            $class = str_replace('_', DIRECTORY_SEPARATOR, $className . '.php');
        }

        if (!empty($this->diretorios)) {
            foreach ($this->diretorios as $diretorio) {
                $classPath = rtrim($diretorio, '/') . DIRECTORY_SEPARATOR . $class;

                if (file_exists($classPath)) {
                    return require_once $classPath;
                }
            }
        }

        if (file_exists($class)) {
            return require_once $class;
        }

        $classPath = stream_resolve_include_path($class);

        if ($classPath !== false) {
            return include_once $classPath;

            throw new Exception("Arquivo {$class} não encontrado");
        }
    }
}
