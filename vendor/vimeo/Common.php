<?php
namespace vimeo;

use DateTime,
    vimeo\PHPMailer;

class Common extends \Exception
{

    static function relatarExcecaoParaAdmin($msg)
    {

    }

    static function returnValor($str)
    {
        return str_replace(",", ".", str_replace(".", "", $str));
    }

    static function encrypt_decrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'foottsdevsis';
        $secret_iv = 'foottsdevsis';

        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    static function validarEmail($email)
    {
        if (!preg_match("/^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$/", $email)) {
            self::gerarMensagem(OPCAOAVISO, AVISO_MSG_EMAILINVALIDO);
        }
    }

    static function tratarCampoTipoArray($dados = [])
    {
        $resultado = '';
        foreach ($dados as $dado) {
            $resultado .= $dado . ',';
        }

        $resultado = rtrim($resultado, ',');

        return $resultado;
    }

    static function retornarPagina($url = "")
    {
        header('Location: ' . SITE_URL . '/' . $url);
        exit;
    }

    static function voltar()
    {
        if (empty($_SERVER['HTTP_REFERER'])) {
            $_SERVER['HTTP_REFERER'] = SITE_URL;
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    static function validarData($date = null)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return ($d && $d->format('d/m/Y') === $date);
    }

    static function validarCampoObrigatorio($dados = [], $apelido = FALSE)
    {
        $msg = 0;
        $campo = "<ul>";
        $i = 0;
        if (!empty($dados)) {
            foreach ($dados as $key => $value) {
                $i++;
                if (empty($value)) {
                    $msg += 1;
                    if ($apelido) {
                        $key = $dados['#APELIDO#'][$i - 1];
                    }
                    $campo .= "<li>" . ucfirst($key) . "</li>";
                }
            }
            $campo = MSG_ALERT_CAMPOS_OBRIGATORIOS . $campo;
        } else {
            $msg = 1;
            $campo = "<ul> ACESSO NÃO PERMITIDO!";
        }

        if ($msg > 0) {
            self::gerarMensagem(OPCAOAVISO, $campo . "</ul>");
        }

        unset($dados['#APELIDO#']);
        return $dados;
    }

    static function StrReplaceMultiplo($parametros, $subject)
    {
        foreach ($parametros as $key => $value) {
            $subject = str_replace($key, $value, $subject);
        }

        return $subject;
    }

    static function validarLink($link)
    {
        return $link;
    }

    static function contarArray($dados = [])
    {
        $total = (sizeof($dados));
        $n = ZERO;
        foreach ($dados as $value) {
            if (empty($value)) {
                $n++;
            }
        }

        return $total - $n;
    }

    static function removerCaracteresEspeciais($nome)
    {
        $string = preg_replace("/[^a-zA-Z0-9_]/", "", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN_"));
        return str_replace('_', ' ', $string);
    }

    static function removerEspacosPontos($nome)
    {
        $string = preg_replace("/[^a-zA-Z0-9]/", "-", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN-"));
        return str_replace('_', '-', $string);
    }

    static function somenteNumeros($valor)
    {
        $string = preg_replace("/[^0-9]/", "", $valor);
        return $string;
    }

    static function checkRemoteFile($url)
    {
        $file_headers = @get_headers($url);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return false;
        } else {
            return true;
        }
    }

    static function formatarData($data)
    {
        return implode('-', array_reverse(explode('/', $data)));
    }

    static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;

        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function validarInputsObrigatorio($dados = [], $redir = null, $modulo = null)
    {
        $qtde = 0;
        $campos = "<ul align='left'>";
        $msg = "Campos obrigatórios não preenchido!";
        $situacao = "danger";
        $acao = 'acao';

        foreach ($dados as $key => $value) {
            if (empty($value)) {

                $qtde += 1;
                $campos .= "<li>" . ucfirst($key) . "</li>";
            }
        }

        $msg .= '<br>' . $campos . "</ul>";

        if ($qtde > 0) {
            self::alert($msg, $situacao, $acao, $modulo);
            self::redir($redir);
        }
    }

    public static function validarAcessoObrigatorio($dados = [], $redir = null)
    {
        $qtde = 0;
        $campos = "<ul align='left'>";
        $msg = "Campos obrigatório não preenchido!";
        $situacao = "alert-danger";
        $acao = 'acao';

        foreach ($dados as $key => $value) {
            if (empty($value)) {

                $qtde += 1;
                $campos .= "<li>" . ucfirst($key) . "</li>";
            }
        }

        $msg .= '<br>' . $campos . "</ul>";

        if ($qtde > 0) {
            self::alert($msg, $situacao, $acao);
            if (empty($redir)) {
                self::voltar();
            } else {
                self::redir($redir);
            }
        }
    }

    public static function validarEmBranco($dado)
    {
        $qtde = 0;

        if (strlen($dado) == 0) {
            $qtde = 1;
        }

        if (($qtde == 1)) {
            $msg = "Campo obrigatório não preenchido!";
            $situacao = "alert-danger";
            $acao = 'acao';
            self::alert($msg, $situacao, $acao);
            self::voltar();
        }
    }

    public static function alert($msg = 'Operação realizada com Sucesso', $situacao = '', $acao = 'acao', $modulo = null)
    {
        if ($situacao == '') {
            $situacao = "success";
        }
        Session::set($acao, TRUE);
        Session::set("acao-class", $situacao);
        Session::set("acao-msg", $msg);

        if ($modulo != null) {
            Session::set("acao-tab", $modulo);
        }
    }

    public static function redir($url = "", $tipo = null)
    {
        if ($tipo == 1) {
            header('location: ' . $url);
        } else {
            header('location: ' . SITE_URL . '/' . $url);
        }
        exit();
    }

    public static function redirExt($url = "")
    {
        header('location: ' . $url);
        exit();
    }

    public static function retornoWSLista($param)
    {
        return $param['list'];
    }

    public static function tratar_frases_grandes($string, $tam = 50)
    {
        $subtam = $tam - 3;
        return strlen(trim($string)) > $tam ? substr($string, 0, $subtam) . '...' : $string;
    }
}
